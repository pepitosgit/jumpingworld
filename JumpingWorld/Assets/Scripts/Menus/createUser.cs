﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class createUser : MonoBehaviour
{
    public InputField emailInput;
    public InputField passInput;
    public Button registerButton;

    public void Start()
    {
        registerButton.onClick.AddListener(() =>
        {
            StartCoroutine(Main.Instance.Web.Register(emailInput.text, passInput.text));
        });
    }

    public void returnMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}