﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuGamePanelWin : MonoBehaviour
{
    public GameObject optionsPanel;


    public void OptionsPanel()
    {
        Time.timeScale = 0;
        optionsPanel.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        optionsPanel.SetActive(false);
    }

    public void ReturnMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuSceneCat");
    }
    public void WinPanel()
    {
        Time.timeScale = 0;
        optionsPanel.SetActive(true);
    }
    public void Continue()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuSceneCat");
    }
}
