﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void login()
    {
        SceneManager.LoadScene("LoginScene");
    }
    public void crearUsuario()
    {
        SceneManager.LoadScene("CreateUserScene");
    }
    public void Exit() 
    {
        Application.Quit();
    }
}
