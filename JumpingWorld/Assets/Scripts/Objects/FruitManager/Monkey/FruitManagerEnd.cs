﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FruitManagerEnd : MonoBehaviour
{
    public GameObject optionsPanel;

    void Update()
    {
        FruitCollected();
    }

    public void FruitCollected()
    {
        if (transform.childCount == 0)
        {
            Time.timeScale = 0;
            optionsPanel.SetActive(true);
        }
    }
}
