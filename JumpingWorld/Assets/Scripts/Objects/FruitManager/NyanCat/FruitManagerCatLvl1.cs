﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FruitManagerCatLvl1 : MonoBehaviour
{
    public int number;

    private void Start()
    {
        number = Random.Range(1, 4);
    }

    void Update()
    {
        FruitCollected();
    }

    public void FruitCollected()
    {
        if (transform.childCount == 0)
        {
            if (number == 1)
            {
                SceneManager.LoadScene("CatLevel2.0");
            }
            if (number == 2)
            {
                SceneManager.LoadScene("CatLevel2.1");
            }
            if (number == 3)
            {
                SceneManager.LoadScene("CatLevel2.2");
            }
        }
    }
}
