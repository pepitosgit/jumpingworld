﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Skins : MonoBehaviour
{

    public GameObject skinsPanel;

    private bool inDoor = false;

    public GameObject player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            skinsPanel.gameObject.SetActive(true);
            inDoor = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        skinsPanel.gameObject.SetActive(false);
        inDoor = false;
    }   

    public void SetPlayerMonkey()
    {
        PlayerPrefs.SetString("PlayerSelected", "Monkey");
        ResetPlayerSkin();
        SceneManager.LoadScene("MenuScene");
    }
    public void SetPlayerNyanCat()
    {
        PlayerPrefs.SetString("PlayerSelected", "NyanCat");
        ResetPlayerSkin();
        SceneManager.LoadScene("MenuSceneCat");
    }
    void ResetPlayerSkin()
    {
        skinsPanel.gameObject.SetActive(false);
        player.GetComponent<PlayerSelect>().ChangePlayerInMenu();
    }
}
