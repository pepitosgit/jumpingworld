﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenDoor : MonoBehaviour
{
    public Text text;  
    public string levelName;
    public int number;
    private bool inDoor = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            
            text.gameObject.SetActive(true);
            inDoor = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        text.gameObject.SetActive(false);
        inDoor = false;
        
    }
    
    private void Start()
    {
        number = Random.Range(1, 4);
    }
    private void Update()
    {
        
        if (inDoor && Input.GetKey("e"))
        {
            if (number == 1)
            {
                SceneManager.LoadScene("SampleScene");
            }
            if (number == 2)
            {
                SceneManager.LoadScene("Lvl1.2");
            }
            if (number == 3)
            {
                SceneManager.LoadScene("Lvl1.3");
            }
        }
    }
    
}
