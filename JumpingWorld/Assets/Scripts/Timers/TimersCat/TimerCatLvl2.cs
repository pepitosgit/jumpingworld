﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerCatLvl2 : MonoBehaviour
{
    public Text tiempoText;
    public float tiempo = 0.0f;
    public int number;
    public GameObject optionsPanel;

    private void Start()
    {
        number = Random.Range(1, 4);
    }

    public void Update()
    {
        tiempo -= Time.deltaTime;
        tiempoText.text = "" + tiempo.ToString("f0");

        if (tiempo <= 0)
        {
            ShowPanel();
        }

    }

    public void ShowPanel()
    {
        optionsPanel.SetActive(true);
    }

    public void Retry()
    {

        if (number == 1)
        {
            SceneManager.LoadScene("CatLevel2.0");
        }
        if (number == 2)
        {
            SceneManager.LoadScene("CatLevel2.1");
        }
        if (number == 3)
        {
            SceneManager.LoadScene("CatLevel2.2");
        }
    }
    public void QuitMenu()
    {
        SceneManager.LoadScene("MenuSceneCat");
    }
}
