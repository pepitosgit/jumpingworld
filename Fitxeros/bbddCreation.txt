create database JumpingWorld;
create table usuari(
	idUsuari integer not null AUTO_INCREMENT,
    email varchar(30),
    pass varchar(20),
    PRIMARY key(idUsuari)
);

create table personatge(
	idPersonatge integer not null,
    idUsuari integer not null,
    vidas integer,
    PRIMARY key(idPersonatge),
    CONSTRAINT fk_id_usuari FOREIGN KEY(idUsuari) REFERENCES usuari (idUsuari)
);

INSERT INTO usuari (idUsuari,email,pass) values(1,'admin@email.com','admin123');
INSERT INTO personatge (idPersonatge, idUsuari) values(1,1);