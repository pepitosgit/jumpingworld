La carpeta JumpingWorld contiene todo el código usado para crear el videojuego en Unity, para poder ejecutar el juego se debe ir a la carpeta JumpingWorldBuild,
pero para que pueda funcionar antes se debe de seguir los pasos explicados a continuación:

-Dentro de la carpeta xampp hay una nueva carpeta llamada jumpingworld, para que el juego pueda funcionar se debe implementar dicha carpeta dentro de htdocs de xampp.
Sin la carpeta el juego no funcionará correctamente, ya que para hacer el login o crear usuario necesita conectarse a una bbdd.

-Para poder crear la bbdd hay que entrar en el fichero bbddCreation, este fichero nos permite crear e implementar datos dentro de nuestra bbdd de mysql.